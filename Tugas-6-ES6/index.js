//soal 1
//buatlah fungsi menggunakan arrow function luas dan keliling persegi panjang dengan arrow function lalu gunakan let atau const di dalam soal ini
const luasPP = (p, l) => {
    let hasil = p * l;
    console.log(hasil)
}
luasPP(2, 3)

const KelilingPP = (p, l) => {
    return 2 * (p + l)
}
console.log(KelilingPP(2, 3))

//soal 2
const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => {
            console.log(firstName + " " + lastName)
        }
    }
}
//Driver Code 
newFunction("William", "Imoh").fullName()

//soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const { firstName, lastName, address, hobby } = newObject
console.log(firstName, lastName, address, hobby)

// soal 4 
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//Driver Code
let combined = [...west, ...east]
console.log(combined);

//soal 5
const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
const theString = `lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log (theString);