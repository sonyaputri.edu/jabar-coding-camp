// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort( ) //jawaban
for (const hewan of daftarHewan) {
    console.log(hewan) }

//soal 2
function introduce (data) {
    console.log('Nama saya '+data.name+', Umur saya '+data.age+', alamat saya di '+data.address+', hobi saya '+data.hobby);
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = { name : "Sonya" , age : 23 , address : "Cimahi" , hobby : "Menggambar"}
//jawaban 
introduce(data); 
introduce (perkenalan);


//soal 3
function hitung_huruf_vokal(nama) { 
    nama = nama.toLowerCase()
    jumlah_vokal = 0
    for (const huruf of nama) {
       if (huruf == "a"|| huruf == "i"|| huruf == "u" || huruf == "e" || huruf == "o") {
           jumlah_vokal++
       }
    }
    return jumlah_vokal
}
var hitung_1 = hitung_huruf_vokal("Ahmad")
var hitung_2 = hitung_huruf_vokal("Aufar")
console.log(hitung_1 , hitung_2);

//soal 5
function hitung (hitung) {
    return 2*hitung-2 
} 
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8

 