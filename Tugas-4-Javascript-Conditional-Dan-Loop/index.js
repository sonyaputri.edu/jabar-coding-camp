// soal 1
var nilai = 75
//pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi
//nilai >= 85 indeksnya A
//nilai >= 75 dan nilai < 85 indeksnya B
//nilai >= 65 dan nilai < 75 indeksnya c
//nilai >= 55 dan nilai < 65 indeksnya D
//nilai < 55 indeksnya E

// jawaban 1
if (nilai >= 85) {
    console.log ("indeksnya A")
} else if (nilai >= 75 && nilai < 85 ) {
    console.log ("indeksnya B")
} else if (nilai >= 65 && nilai < 75) {
    console.log ("indeksnya C")
} else if (nilai >= 55 && nilai < 65) {
    console.log ("indeksnya D")
} else if (nilai <55 ) {
    console.log ("indeksnya E")
}

// soal 2 
var tanggal = 13;
var bulan = 10;
var tahun = 1998;
// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case 
//pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 
//(isi di sesuaikan dengan tanggal lahir masing-masing)

//Jawaban 2
switch(bulan) {
    case 1:   { bulan='Januari'; break; }
    case 2:   { bulan='Februari'; break; }
    case 3:   { bulan='Maret'; break; }
    case 4:   { bulan='April' ; break; }
    case 5:   { bulan='Mei'; break; }
    case 6:   { bulan='Juni'; break; }
    case 7:   { bulan='Juli'; break; }
    case 8:   { bulan='Agustus' ;break; }
    case 9:   { bulan='September'; break; }
    case 10:   { bulan='Oktober'; break; }
    case 11:   { bulan='November'; break; }
    case 12:   { bulan='Desember'; break; }}
  console.log(tanggal+" "+bulan+" "+tahun)



//soal 3
//Kali ini kamu diminta untuk menampilkan sebuah segitiga 
//dengan tanda pagar (#) dengan dimensi tinggi n dan alas n.

// jawaban 3 pakai cara digoogle
var n = 3 //alas
var mulai = 1 //tinggi
var p = "#"
while (mulai <= n ) {
    console.log(p.repeat(mulai));
    mulai++
}
// jawaban n=7 pakai cara clue pas live
//var n = tinggi
//var j = alas
var segitiga = ''
for (var n = 0; n < 7; n++) {
    for (var j = 0; j <= n ; j++){
        segitiga += '#'
    }
    segitiga += '\n';
} 
console.log (segitiga);


// soal 4
//jawaban 4 output m = 3
var m = 10
var mulai = 0
var p = "="
while ( mulai < m) {
if ( mulai % 3 == 0 ) { console.log(mulai+1 + "-" +"I love programming")}
    else if ( mulai % 3 == 1 ) { console.log(mulai+1 + "-" + "I love Javascript")}
    else if ( mulai % 3 == 2 )  { 
        console.log(mulai+1 + "-"+ "I love VueJS")
        console.log(p.repeat(mulai+1))
    }
    mulai++
}
