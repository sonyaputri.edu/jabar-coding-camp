// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
//jawaban soal 1
var pertama1 = pertama.substring(0,4);
var pertama2 = pertama.substring(12,18);
var kedua1 = kedua.substring(0,7);
var kedua2 = kedua.substring(8,18).toUpperCase();

console.log(pertama1+" "+pertama2+" "+kedua1+" "+kedua2);


// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
// jawaban soal 2
var angka1 = parseInt(kataPertama);
var angka2 = parseInt(kataKedua);
var angka3 = parseInt(kataKetiga);
var angka4 = parseInt (kataKeempat);

console.log(angka1+angka2*angka3+angka4);


// soal 3
var kalimat = 'wah javascript itu keren sekali'; 
// jawaban soal 3
var kataPertama = kalimat.substring(0, 3); 
var kataKedua  = kalimat.substring(4,15);
var kataKetiga = kalimat.substring(15,19);
var kataKeempat = kalimat.substring(19,25);
var kataKelima = kalimat.substring(25,31);

console.log ('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata ketiga: '+ kataKetiga);
console.log('Kata Keempat: '+ kataKeempat);
console.log('Kata Kelima: '+ kataKelima);